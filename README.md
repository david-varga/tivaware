# TivaWare DriverLib
This library implements the basic Board Support Package functions for the Tiva C TM4C1294 Connected Launchpad.

The code is provided by TI under a 3-clause BSD Licence.

## Building driverlib
### Stand-alone build
```
mkdir build && cd build
cmake -DCMAKE_TOOLCHAIN_FILE=../TM4C129.cmake ..
make
```


### Build within another  CMake project
TBD